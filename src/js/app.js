App = {
  web3Provider: null,
  contracts: {},
  account: 0x0,
  loading: false,

  init: function() {
    return App.initWeb3();
  },

  initWeb3: function() {
    // Initialize web3 and set the provider to the testRPC.
    if (typeof web3 !== 'undefined') {
      App.web3Provider = web3.currentProvider;
      web3 = new Web3(web3.currentProvider);
    } else {
      // set the provider you want from Web3.providers
      App.web3Provider = new Web3.providers.HttpProvider('http://localhost:8545');
      web3 = new Web3(App.web3Provider);
    }
    App.displayAccountInfo();
    return App.initContract();
  },

  displayAccountInfo: function() {
    web3.eth.getCoinbase(function(err, account) {
      if (err === null) {
        App.account = account;
        $("#account").text(account);
        web3.eth.getBalance(account, function(err, balance) {
          if (err === null) {
            $("#accountBalance").text(web3.fromWei(balance, "ether") + " ETH");
          }
        });
      }
    });
  },

  initContract: function() {
    $.getJSON('ChainList.json', function(chainListArtifact) {
      // Get the necessary contract artifact file and use it to instantiate a truffle contract abstraction.
      App.contracts.ChainList = TruffleContract(chainListArtifact);

      // Set the provider for our contract.
      App.contracts.ChainList.setProvider(App.web3Provider);

      // Listen for events
      App.listenToEvents();

      // Retrieve the article from the smart contract
      return App.reloadArticles();
    });
  },

  reloadArticles: function() {

    if (App.loading) {
          return;
        }
    App.loading = true;

    // refresh account information because the balance may have changed
    App.displayAccountInfo();

    App.contracts.ChainList.deployed().then(function(instance) {
      chainListInstance = instance;
      return chainListInstance.getActualBets();
    }).then(function(betIds) {

      // Retrieve and clear the article placeholder
      var articlesRow = $('#articlesRow');
      articlesRow.empty();

      for (var i = 0; i < betIds.length; i++) {
        var betId = betIds[i];
        chainListInstance.bets(parseInt(betId)).then(function(article) {
          // Retrieve and fill the article template

          console.log("article[0]="+article[0]);
          console.log("article[1]="+article[1]);
          console.log("article[2]="+article[2]);
          console.log("article[3]="+article[3]);

          var articleTemplate = $('#articleTemplate');
          articleTemplate.find('.panel-title').text(article[2]);
          articleTemplate.find('.article-description').text(article[3]);
          articleTemplate.find('.article-price').text(web3.fromWei(article[4], "ether"));
          articleTemplate.find('.article-koef1').text(article[5]);
          articleTemplate.find('.article-koef2').text(article[6]);
          articleTemplate.find('.article-bet').text(web3.fromWei(article[8], "ether"));
          var bet = article[8];
          if (bet == 0x0) {
            console.log("bet == 0");
          } else {
            console.log("bet != 0");
          }
          var buttonBlock =  articleTemplate.find('#buttonBlock');
          buttonBlock.hide();
          var labelBet = articleTemplate.find('#labelBet');
          labelBet.hide();

          var seller = article[1];
          if (seller == App.account) {
            seller = "You";
            var betdetail =   articleTemplate.find('#betdetail');
            betdetail.hide();
            var delButtonBlock =   articleTemplate.find('#delButtonBlock');
            delButtonBlock.show();
          }
          else {
            var delButtonBlock =  articleTemplate.find('#delButtonBlock');
            delButtonBlock.hide();
            var betdetail =   articleTemplate.find('#betdetail');
            betdetail.show();

          }

          articleTemplate.find('.article-seller').text(seller);

          var makebet = article[7];
          console.log ("makebet:" + makebet);
          var betterAddress = article[7];
          if (makebet == 0x0) {
            articleTemplate.find('#betButton').show();
            makebet = "No one place a bet yet";
            betterAddress = 0x0;
          }
          else {

            if (makebet == App.account){
               makebet = "You";
               betterAddress = 0x0;
            } else {
               var delButtonBlock =  articleTemplate.find('#delButtonBlock');
               delButtonBlock.hide();
            }
            articleTemplate.find('#betButton').hide();
            if (article[9]==0) {
              console.log ('article[9]='+article[9]);
              var buttonBlock =  articleTemplate.find('#buttonBlock');
              buttonBlock.show();
            }
            else {
              console.log ('bet closed');
              var labelBet =  articleTemplate.find('#labelBet');
              labelBet.show();
            }
          }
          articleTemplate.find('.article-makebet').text(makebet);
          articleTemplate.find('.btn-loose').attr('data-id', article[0]);
          articleTemplate.find('.btn-loose').attr('data-value', bet*article[6]);
          articleTemplate.find('.btn-win').attr('data-id', article[0]);
          articleTemplate.find('.btn-win').attr('data-value', article[4]*article[5]);
          articleTemplate.find('.btn-del').attr('data-id', article[0]);
          console.log('btn-del:'+  articleTemplate.find('.btn-del'));
          console.log('btn-del data:'+  articleTemplate.find('.btn-del').data('id'));
          var makebetButton = articleTemplate.find('.btn-makebet');
          articleTemplate.find('.btn-makebet').attr('data-id', article[0]);
          articleTemplate.find('.btn-makebet').attr('data-price', article[4]*article[5]/article[6]);
          console.log('makebetButton:'+makebetButton+':'+ (typeof makebetButton != "undefined"));
          console.log('makebetButton data:'+ articleTemplate.find('.btn-makebet').data('id'))


          // add this new article
          articlesRow.append(articleTemplate.html());
        });
      }
      App.loading = false;
    }).catch(function(err) {
      console.log(err.message);
      App.loading = false;
    });
  },

  sellArticle: function() {
    // retrieve details of the article
    var _article_name = $("#article_name").val();
    var _description = $("#article_description").val();
    var _price = web3.toWei(parseInt($("#article_price").val() || 0), "ether");
    var _koef1 = $("#article_koef1").val();
    var _koef2 = $("#article_koef2").val();

    if ((_article_name.trim() == '') || (_price == 0)) {
      // nothing to sell
      return false;
    }

    App.contracts.ChainList.deployed().then(function(instance) {
      return instance.sellArticle(_article_name, _description, _price, _koef1, _koef2, {
        from: App.account,
        gas: 500000
      });
    }).then(function(result) {

    }).catch(function(err) {
      console.error(err);
    });
  },

  addArticle: function() {
    var betId = parseInt($(event.target).data('id'));
    var price = parseInt($(event.target).data('price'));
    console.log('price:'+price)
    // retrieve details of the article
    var _bet = web3.toWei(parseInt($("#article_amount").val() || 0), "ether");
    if (_bet == 0) {
      window.alert("Please, enter positive amount of ETH");
      return false;
    }
    else if (price < _bet) {
    var textprice = Number(price).toFixed(2);
    window.alert("Your bet must be more then or equal to "+ textprice +" ETH");
      return false;
    }
    console.log('betid:' + betId);
    App.contracts.ChainList.deployed().then(function(instance) {
      return instance.addArticle(betId, _bet, {
        from: App.account,
        gas: 500000
      });
    }).then(function(result) {

    }).catch(function(err) {
      console.error(err);
    });

  },

  // Listen for events raised from the contract
  listenToEvents: function() {
    App.contracts.ChainList.deployed().then(function(instance) {
      instance.sellArticleEvent({}, {
        fromBlock: 0,
        toBlock: 'latest'
      }).watch(function(error, event) {
        var status;
        if (event.args._betEnded != 0){
          status = 'is close';
        }else {
          status = 'is open';
        }
        $("#events").append('<li class="list-group-item">' + event.args._name + ' ' + status + '</li>');
        App.reloadArticles();
      });
    });
  },

  endBet: function(isBetWin) {
    event.preventDefault();
    var id = parseInt($(event.target).data('id'));
    // retrieve the article price
    var _price = parseInt($(event.target).data('value'));

    if (isBetWin==0){
    App.contracts.ChainList.deployed().then(function(instance) {
      return instance.looseBet(id,{
        from: App.account,
        value: _price,
        gas: 500000
      });
    }).then(function(result) {

    }).catch(function(err) {
      console.error(err);
    });
    }
    else {
      App.contracts.ChainList.deployed().then(function(instance) {
        return instance.getBetterByBetID(id);
      }).then(function(payer) {
        if (payer == 0x0) {
          return false;
        }
        else {
          App.contracts.ChainList.deployed().then(function(instance) {
            return instance.winBet(id,{
              from: payer,
              value: _price,
              gas: 500000
            });
            }).then(function(result) {
              }).catch(function(err) {
                    console.error(err);
                  });
        }
      });
    }
  },
  deleteBet: function(){
    var id = parseInt($(event.target).data('id'));
    App.contracts.ChainList.deployed().then(function(instance) {
      return instance.deleteBet(id, App.account, {
        from: App.account,
        gas: 500000
      });
    }).then(function(result) {
      console.log('Bet deleted');
      var delButtonBlock = $('#delButtonBlock');
      delButtonBlock.hide();
      var labelBet = $('#labelBet');
      labelBet.show();
    }).catch(function(err) {
              console.error(err);
    });
  }
};




$(function() {
  $(window).load(function() {
    App.init();
  });
});

$('#addabet').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var betID = button.data('id') // Extract info from data-* attributes
  var price = button.data('price') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
  modal.find('.btn-addBet').attr('data-id', betID);
  modal.find('.btn-addBet').attr('data-price', price);
  modal.find('.makebet-subtitle').text(Number(web3.fromWei(price, "ether")).toFixed(2));
});
