pragma solidity ^0.4.11;

contract ChainList {
    // State variables
    struct Bet{
      uint id;
      address seller;
      string name;
      string description;
      uint256 price;
      uint256 koef1;
      uint256 koef2;
      address makebet;
      uint256 bet;
      uint256 betEnded;
    }

    address owner;
    mapping(uint => Bet) public bets;
    uint betCounter;

    // Events
    event sellArticleEvent (
          uint indexed _id,
          address indexed _seller,
          string _name,
          uint256 _price,
          uint256 _koef1,
          uint256 _koef2,
          address indexed _makebet,
          uint256 _bet,
          uint256 _betEnded
    );

    // sell an article
    function sellArticle(string _name, string _description, uint256 _price, uint _koef1, uint _koef2) public {
      betCounter++;

      // store this article
      bets[betCounter] = Bet(
           betCounter,
           msg.sender,
           _name,
           _description,
           _price,
           _koef1,
           _koef2,
           0x0,
           0,//bet
           0//bet ended
      );

        sellArticleEvent(betCounter, msg.sender, _name, _price, _koef1, _koef2, 0x0, 0, 0);
    }

    function winBet(uint id) payable public {

      require(id > 0 && id <= betCounter );

      // we check whether there is an article for sale
      require(bets[id].seller != 0x0);

      // we check that the article was not already sold
      require(bets[id].makebet != 0x0);

      require((bets[id].price*bets[id].koef1) == msg.value);

      require(bets[id].betEnded == 0);

      // the buyer can buy the article
      bets[id].seller.transfer(msg.value);

      bets[id].betEnded = 1;
      sellArticleEvent(id, bets[id].seller, bets[id].name, bets[id].price, bets[id].koef1, bets[id].koef2, bets[id].makebet, bets[id].bet, bets[id].betEnded);

    }
    function looseBet(uint id) payable public {
      // we check whether there is an article for sale

      require(id > 0 && id <= betCounter );

      require(bets[id].seller != 0x0);

      // we check that the article was not already sold
      require(bets[id].makebet != 0x0);

      require((bets[id].bet*bets[id].koef2) == msg.value);

      require(bets[id].betEnded == 0);

      // the buyer can buy the article
      bets[id].makebet.transfer(msg.value);

      bets[id].betEnded = 1;
      sellArticleEvent(id, bets[id].seller, bets[id].name, bets[id].price, bets[id].koef1, bets[id].koef2, bets[id].makebet, bets[id].bet, bets[id].betEnded);

    }
    // sell an article
    function addArticle(uint index, uint256 _bet) public {
        require(index > 0 && index <= betCounter );
        require(bets[index].makebet == 0x0);
        require(bets[index].price*bets[index].koef1 >= _bet*bets[index].koef2);
        bets[index].makebet = msg.sender;
        bets[index].bet = _bet;
        sellArticleEvent(index, bets[index].seller, bets[index].name, bets[index].price, bets[index].koef1, bets[index].koef2, bets[index].makebet, bets[index].bet, bets[index].betEnded);
        //addArticleEvent(seller, name, price, koef1, koef2, bet);
    }

    // fetch the number of articles in the contract
    function getNumberOfBets() public constant returns (uint) {
      return betCounter;
    }

    // fetch and returns all article IDs available for sale
    function getActualBets() public constant returns (uint[]) {
      // we check whether there is at least one article
      if(betCounter == 0) {
        return new uint[](0);
      }

      // prepare intermediary array
      uint[] memory betsIds = new uint[](betCounter);

      uint numberOfBets = 0;
      // iterate over articles
      for (uint i = 1; i <= betCounter; i++) {
        // keep only the ID of articles not sold yet
        if (bets[i].betEnded == 0) {
          betsIds[numberOfBets] = bets[i].id;
          numberOfBets++;
        }
      }

      // copy the articleIds array into the smaller forSale array
      uint[] memory retVal = new uint[](numberOfBets);
      for (uint j = 0; j < numberOfBets; j++) {
        retVal[j] = betsIds[j];
      }
      return (retVal);
    }

    function getBetterByBetID(uint index) public constant returns (address) {

      require(index > 0 && index <= betCounter );
      require(bets[index].makebet!=0x0);
      require(bets[index].betEnded==0);
      return bets[index].makebet;
    }
    function deleteBet(uint index, address requestedAddress) public {
      require(index > 0 && index <= betCounter );

      require(bets[index].seller == requestedAddress);
      bets[index].seller = 0x0;
      bets[index].description = '';
      bets[index].price = 0;
      bets[index].koef1 = 0;
      bets[index].koef2 = 0;
      bets[index].makebet = 0x0;
      bets[index].betEnded = 1;
      sellArticleEvent(index, bets[index].seller, bets[index].name, bets[index].price, bets[index].koef1, bets[index].koef2, bets[index].makebet, bets[index].bet, bets[index].betEnded);
      bets[index].name = '';
    }
}
